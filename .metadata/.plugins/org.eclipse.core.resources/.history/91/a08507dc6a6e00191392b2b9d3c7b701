package com.example.demo;

import java.math.BigDecimal;

/**
 * Author: Luca Visentin
 * Date: 04/05/2019
 * Client: DocShifter
 * Description: Create a RESTful Service (Calculator) that allows to do basic operations like sum, subtract, multiply, divide
 * 				and exponentiation between two values passed in parameters.
 */
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Greetings {
	
	private String ret = "";
	
	@RequestMapping("/operations")
	public @ResponseBody String operation(@RequestParam(value="value1", defaultValue="1.0") String value1,  
										   @RequestParam(value="value2", defaultValue="1.0") String value2, 
										   @RequestParam(value="operation", defaultValue="") String operation) {
		
		operation = operation.toLowerCase();
		switch(operation) { 
			case "sum": 	 ret = sum(value1, value2);
				break;
			case "subtract": ret = subtract(value1, value2);
				break;
			case "multiply": ret = multiply(value1, value2);
				break;
			case "divide":   ret = divide(value1, value2);
				break;
			case "exponentiation": ret = exponentiation(value1, value2);
				break;
			default: ret = "No operation selected";
		}
		
		return ret;

	}
	
	private String sum(String value1, String value2) {
		//Initialize is equal to zero because is the neutral value for the sum operation
		double dValue1 = 0, dValue2 = 0;
		 String ret = "";
		
		try{
			dValue1 =Double.valueOf(value1);
		} catch(NumberFormatException nfe) {
			ret = "First value is not a valid number \n";
		}

		try{
			dValue2 = Double.valueOf(value2);
		} catch(NumberFormatException nfe) {
			ret = "Second value is not a valid number \n";
		}
		
		if(ret.length() == 0) {
			double result = dValue1+dValue2; 
			
			ret = "The sum between " + value1 + " and " + value2 + " is " + result;
		}
		
		return ret;
	}
	
	private String subtract(String value1, String value2) {
		//Initialize is equal to zero because is the neutral value for the subtract operation
		double dValue1 = 0, dValue2 = 0;
		String ret = "";
		
		try{
			dValue1 =Double.valueOf(value1);
		} catch(NumberFormatException nfe) {
			ret = "First value is not a valid number \n";
		}

		try{
			dValue2 = Double.valueOf(value2);
		} catch(NumberFormatException nfe) {
			ret = "Second value is not a valid number \n";
		}
		
		if(ret.length() == 0) {
			double result = dValue1-dValue2; 
			
			ret = "The subtract between " + value1 + " and " + value2 + " is " + result;
		}
		
		return ret;
	}
	
	private String multiply(String value1, String value2) {
		//Initialize is equal to one because is the neutral value for the multiply operation
		double dValue1 = 1, dValue2 = 1;
		String ret = "";
		
		try{
			dValue1 =Double.valueOf(value1);
		} catch(NumberFormatException nfe) {
			ret = "First value is not a valid number \n";
		}

		try{
			dValue2 = Double.valueOf(value2);
		} catch(NumberFormatException nfe) {
			ret = "Second value is not a valid number \n";
		}
		
		if(ret.length() == 0) {
			double result = dValue1*dValue2; 
			
			ret = "The multiply between " + value1 + " and " + value2 + " is " + result;
		}
		
		return ret;
	}
	
	private String divide(String value1, String value2) {
		//Initialize is equal to one because is the neutral value for the divide operation
		double dValue1 = 1, dValue2 = 1;
		String ret = "";
		
		try{
			dValue1 =Double.valueOf(value1);
		} catch(NumberFormatException nfe) {
			ret = "First value is not a valid number \n";
		}

		try{
			dValue2 = Double.valueOf(value2);
			if(dValue2 == 0)
				ret = "It's impossibile divide a number by 0";
			
		} catch(NumberFormatException nfe) {
			ret = "Second value is not a valid number \n";
		}
		
		if(ret.length() == 0) {
			double result = dValue1/dValue2; 
			
			ret = "The divide between " + value1 + " and " + value2 + " is " + result;
		}
		
		return ret;
	}
	
	private String exponentiation(String value1, String value2) {

		//Initialize is equal to one because is the neutral value for the divide operation
		double dValue1 = 1, dValue2 = 1;
		String ret = "";
		
		try{
			dValue1 =Double.valueOf(value1);
		} catch(NumberFormatException nfe) {
			ret = "First value is not a valid number \n";
		}

		try{
			dValue2 = Double.valueOf(value2);
			
		} catch(NumberFormatException nfe) {
			ret = "Second value is not a valid number \n";
		}
		
		if(ret.length() == 0) {
			BigDecimal result = new BigDecimal(Math.pow(dValue1,dValue2)); 
			
			ret = "The exponentiation between " + value1 + " and " + value2 + " is " + result.toString();
		}
		
		return ret;
	}

	//Method to calculate Fibonacci's sequence until the value passed
	@RequestMapping("/fibonacciSequence")
	public @ResponseBody String fibonacci(@RequestParam(value="value1", defaultValue="1.0") String value1) {
		double dValue1 = 0;
		try{
			dValue1 = Double.valueOf(value1);
		}catch(NumberFormatException nfe) {
			ret = "The value passed is not a valid number";
		}
		
		ret = calculateFibonacciSequence(dValue1);
		
		return ret;

	}
	
	private String calculateFibonacciSequence(double value1) {
		String ret = "Fibonacci Sequence until " + value1 + " is: ";
		int prec1 = 1;
		int prec2 = 2;
		
		if(value1 >= 2)
			ret += prec1 + ", " + prec2;
		else
			ret = "" + prec1;
		
		int number = prec1+prec2;
		while (number < value1) {
			ret += ", " + number;
			
			prec1 = prec2;
			prec2 = number;
			
			number = prec1+prec2;
		}
		
		return ret;
	}
}
