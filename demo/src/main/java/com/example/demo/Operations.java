package com.example.demo;

/**
 * Author: Luca Visentin
 * Date: 04/05/2019
 * Client: DocShifter
 * Description: Create a RESTful Service (Calculator) that allows to do basic operations like sum, subtract, multiply, divide
 * 				and exponentiation between two values passed in parameters.
 */

import java.math.BigDecimal;
import java.text.DecimalFormat;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Operations {
	
	private String ret = null;
	
	@RequestMapping("/operations")
	public @ResponseBody String operation(@RequestParam(value="value1", defaultValue="0.0") String value1,  
										   @RequestParam(value="value2", defaultValue="0.0") String value2, 
										   @RequestParam(value="operation", defaultValue="") String operation) {
		//Choice about operation using one method and 3 parameters
		ret = "";
		operation = operation.trim().toLowerCase();
		switch(operation) {
			case "+":
			case "sum": 	 ret = sum(value1, value2);
				break;
			case "-":
			case "subtract": ret = subtract(value1, value2);
				break;
			case "*":
			case "multiply": ret = multiply(value1, value2);
				break;
			case "/":
			case "divide":   ret = divide(value1, value2);
				break;
			case "^":
			case "exponentiation": ret = exponentiation(value1, value2);
				break;
			default: ret = "No operation selected";
		}
		
		return ret;

	}
	
	private String sum(String value1, String value2) {
		//Initialize is equal to zero because is the neutral value for the sum operation
		double dValue1 = 0, dValue2 = 0;
		String ret = "";
		
		//Checking the number format
		try{
			dValue1 =Double.valueOf(value1);
		} catch(NumberFormatException nfe) {
			ret = "First value is not a valid number \n";
		}
		//Checking the number format
		try{
			dValue2 = Double.valueOf(value2);
			
		} catch(NumberFormatException nfe) {
			ret += "Second value is not a valid number \n";
		}
		//If the ret variable is empty, so the numbers are correctly formed, I will calculate the addition
		if(ret.length() == 0) {
			BigDecimal result = new BigDecimal(dValue1+dValue2);
			ret = "The addition between " + value1 + " and " + value2 + " is " + result;
		}
		
		return ret;
	}
	
	private String subtract(String value1, String value2) {
		//Initialize is equal to zero because is the neutral value for the subtract operation
		double dValue1 = 0, dValue2 = 0;
		String ret = "";
		
		//Checking the number format
		try{
			dValue1 =Double.valueOf(value1);
		} catch(NumberFormatException nfe) {
			ret = "First value is not a valid number \n";
		}
		//Checking the number format
		try{
			dValue2 = Double.valueOf(value2);
			
		} catch(NumberFormatException nfe) {
			ret += "Second value is not a valid number \n";
		}
		//If the ret variable is empty, so the numbers are correctly formed, I will calculate the subtraction
		
		if(ret.length() == 0) {
			BigDecimal result = new BigDecimal(dValue1-dValue2);
			ret = "The subtraction between " + value1 + " and " + value2 + " is " + result.toString();
		}
		
		return ret;
	}
	
	private String multiply(String value1, String value2) {
		//Initialize is equal to one because is the neutral value for the multiply operation
		double dValue1 = 1, dValue2 = 1;
		String ret = "";
		
		//Checking the number format
		try{
			dValue1 =Double.valueOf(value1);
		} catch(NumberFormatException nfe) {
			ret = "First value is not a valid number \n";
		}
		//Checking the number format
		try{
			dValue2 = Double.valueOf(value2);
			
		} catch(NumberFormatException nfe) {
			ret += "Second value is not a valid number \n";
		}
		//If the ret variable is empty, so the numbers are correctly formed, I will calculate the multiplication		
		if(ret.length() == 0) {
			BigDecimal result = new BigDecimal(dValue1*dValue2); 
			ret = "The multiplication between " + value1 + " and " + value2 + " is " + result.toString();
		}
		
		return ret;
	}
	
	private String divide(String value1, String value2) {
		//Initialize is equal to one because is the neutral value for the divide operation
		double dValue1 = 1, dValue2 = 1;
		String ret = "";
		//Checking the number format
		try{
			dValue1 =Double.valueOf(value1);
		} catch(NumberFormatException nfe) {
			ret = "First value is not a valid number \n";
		}
		//Checking the number format
		try{
			dValue2 = Double.valueOf(value2);
			if(dValue2 == 0)
				ret = "It's impossibile divide a number by 0";
		} catch(NumberFormatException nfe) {
			ret += "Second value is not a valid number \n";
		}
		
		//If the ret variable is empty, so the numbers are correctly formed, I will calculate the division
		if(ret.length() == 0) {
			BigDecimal result = new BigDecimal(dValue1/dValue2); 
			ret = "The division between " + value1 + " and " + value2 + " is " + result.toString();
		}
		
		return ret;
	}
	
	private String exponentiation(String value1, String value2) {

		//Initialize is equal to one because is the neutral value for the divide operation
		double dValue1 = 1, dValue2 = 1;
		String ret = "";
		//Checking the number format
		try{
			dValue1 =Double.valueOf(value1);
		} catch(NumberFormatException nfe) {
			ret = "First value is not a valid number \n";
		}
		//Checking the number format
		try{
			dValue2 = Double.valueOf(value2);
			
		} catch(NumberFormatException nfe) {
			ret += "Second value is not a valid number \n";
		}
		//If the ret variable is empty, so the numbers are correctly formed, I will calculate the pow		
		if(ret.length() == 0) {
			try{
				BigDecimal result = new BigDecimal(Math.pow(dValue1,dValue2));
				ret = "The exponentiation between " + value1 + " and " + value2 + " is " +  result.toString();
			} catch(NumberFormatException nfe) {
				ret = "The values are too bigger to do exponentiation operation, the value is inifinity or NaN";
			}
			
		}
		
		return ret;
	}

	//Method to calculate Fibonacci's sequence until the value passed
	@RequestMapping("/fibonacciSequence")
	public @ResponseBody String fibonacci(@RequestParam(value="value1", defaultValue="0.0") String value1) {
		double dValue1 = 0;
		ret = "";
		//Check about passed value
		try{
			dValue1 = Double.valueOf(value1);
		}catch(NumberFormatException nfe) {
			ret = "The value passed is not a valid number";
		}
		//If ret variable is empty I will do the method
		if(ret.length() == 0 && dValue1 > 0)
			ret = calculateFibonacciSequence(dValue1);
		else
			ret = "The value " + value1 + " cannot generate a Fibonacci Sequence";
		return ret;

	}
	
	private String calculateFibonacciSequence(double value1) {
		//Response
		String ret = "Fibonacci Sequence until " + value1 + " is: ";
		//Initialize of 2 values used for Fibonacci sequence
		int prec1 = 1;
		int prec2 = 2;
		//Check if Value is GE 2 i will insert both the values
		if(value1 >= 2)
			ret += prec1 + ", " + prec2;
		//else if is only GE 1 I will insert only my first value
		else if(value1 >= 1)
			ret += "" + prec1;
		//Sum to check if the first value of the sequence is LT value passed
		int number = prec1+prec2;
		//Calculation of Fibonacci Sequence
		while (number < value1) {
			ret += ", " + number;
			
			prec1 = prec2;
			prec2 = number;
			
			number = prec1+prec2;
		}
		
		return ret;
	}
}
